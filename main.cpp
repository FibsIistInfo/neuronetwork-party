#include <iostream>
#include "Neuron.h"
#include "Synaps.h"

using namespace std;

int main()
{
    double resultWeights[2] = {-1.0, 1.0};
    Neuron* resultNeuron = new Neuron(2, resultWeights, 0, nullptr);

    double hidden1Weights[3] = {.25, .25, 0};
    Synaps* hidden1Axons[1] = {resultNeuron->getSynaps(0)};
    Neuron* hidden1Neuron = new Neuron(3, hidden1Weights, 1, hidden1Axons);

    double hidden2Weights[3] = {.5, -0.4, 0.9};
    Synaps* hidden2Axons[1] = {resultNeuron->getSynaps(1)};
    Neuron* hidden2Neuron = new Neuron(3, hidden2Weights, 1, hidden2Axons);

    double vodkaWeights[1] = {1.0};
    Synaps* vodkaAxons[2] = {hidden1Neuron->getSynaps(0), hidden2Neuron->getSynaps(0)};
    Neuron* vodkaNeuron = new Neuron(1, vodkaWeights, 2, vodkaAxons);

    double rainWeights[1] = {1.0};
    Synaps* rainAxons[2] = {hidden1Neuron->getSynaps(1), hidden2Neuron->getSynaps(1)};
    Neuron* rainNeuron = new Neuron(1, rainWeights, 2, rainAxons);

    double friendWeights[1] = {1.0};
    Synaps* friendAxons[2] = {hidden1Neuron->getSynaps(2), hidden2Neuron->getSynaps(2)};
    Neuron* friendNeuron = new Neuron(1, friendWeights, 2, friendAxons);

    vodkaNeuron->setInput(0, 1)->run();
    rainNeuron->setInput(0, 0)->run();
    friendNeuron->setInput(0, 0)->run();

    cout << "vodka" << endl;
    cout << vodkaNeuron->getSynaps(0)->result() << endl;
    cout << "rain" << endl;
    cout << rainNeuron->getSynaps(0)->result() << endl;
    cout << "friend" << endl;
    cout << friendNeuron->getSynaps(0)->result() << endl;

    cout << "hidden 1" << endl;
    cout << hidden1Neuron->getSynaps(0)->result() << endl;
    cout << hidden1Neuron->getSynaps(1)->result() << endl;
    cout << hidden1Neuron->getSynaps(2)->result() << endl;
    cout << " output " << hidden1Neuron->getOutput() << endl;
    cout << "hidden 2" << endl;
    cout << hidden2Neuron->getSynaps(0)->result() << endl;
    cout << hidden2Neuron->getSynaps(1)->result() << endl;
    cout << hidden2Neuron->getSynaps(2)->result() << endl;
    cout << " output " << hidden2Neuron->getOutput() << endl;

    cout << "result" << endl;
    cout << resultNeuron->getSynaps(0)->result() << endl;
    cout << resultNeuron->getSynaps(1)->result() << endl;

    cout << "result output" << endl;
    cout << resultNeuron->getOutput() << endl;


    return 0;
}
