#include "Neuron.h"
#include "Synaps.h"
#include "Activation.h"

double Neuron::getSum() {
    sum = 0;
    for (int i=0; i<this->inputCount; i++) {
        sum += synaps[i]->result();
    }
    return sum;
}

double Neuron::getOutput() {
    this->output = Activation::stepActivationFunction(this->getSum());
    return this->output;
};

Neuron* Neuron::run()
{
    this->getOutput();
    if (outputCount <= 0 || this->axon == nullptr)
        return this;
    for(int i=0; i<outputCount; i++){
        this->axon[i]->setInput(this->output);
        if (this->axon[i]->getNeuron() != nullptr) {
            this->axon[i]->getNeuron()->run();
        }
    }
    return this;
}


Synaps* Neuron::getSynaps(int index)
{
    return index<inputCount?synaps[index]:nullptr;
}

Neuron* Neuron::setInput(int index, double value)
{
    if (index>=0 && index<this->inputCount)
        this->getSynaps(index)->setInput(value);
    this->getOutput();
    return this;
}


Neuron::Neuron(int inputCount, double weights[], int outputCount, Synaps** axon) {
    this->inputCount = inputCount;
    this->outputCount = outputCount;
    this->synaps = new Synaps*[inputCount];
    for (int i=0;i<inputCount;i++) {
        this->synaps[i] = new Synaps(this, weights[i]);
    }
    if (outputCount > 0 && axon != nullptr) {
       this->axon = axon;
    }
}
