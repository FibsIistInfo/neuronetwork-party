#pragma once

class Synaps;


class Neuron {
protected:
    int inputCount;
    Synaps** synaps = nullptr;
    double sum = 0;
    double output = 0;
    int outputCount;
    Synaps** axon = nullptr;

public:
    Neuron(int inputCount, double weights[], int outputCount, Synaps** axon);
    Synaps* getSynaps(int index);
    Neuron* setInput(int index, double value);
    Neuron* run();
    double getSum();
    double getOutput();

};

