#include "Synaps.h"


Synaps::Synaps(Neuron* neuron, double weight) {
    this->neuron = neuron;
    this->weight = weight;
}

void Synaps::setInput(double input) {
    this->input = input;
}

double Synaps::getInput() {
    return input;
}
double Synaps::getWeight() {
    return weight;
}
double Synaps::result() {
    return input*weight;
}

Neuron* Synaps::getNeuron() {
    return neuron;
}
