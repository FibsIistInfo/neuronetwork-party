#pragma once
class Neuron;

class Synaps {
protected:
    double weight = 0;
    double input = 0;
    Neuron* neuron = nullptr;

public:

    Synaps(Neuron* neuron, double weight);
    void setInput(double input);
    double getInput();
    double getWeight();
    double result();
    Neuron* getNeuron();
};
